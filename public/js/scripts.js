
var request = new XMLHttpRequest()

request.open("GET", "https://ghibliapi.herokuapp.com/films", true)
request.onload = function () {

    var respuesta = JSON.parse(this.response)
    var app = document.getElementById('root')

    //la llamada se realizo correctamente

    var contenedor = document.createElement('div');
    contenedor.classList.add('container');
    app.appendChild(contenedor);

    var logo = document.createElement('img');
    logo.src = 'https://es.freelogodesign.org/Content/img/logo-ex-2.png';
    app.appendChild(logo);

    respuesta.forEach(function (objeto, indice){

    var pelicula = document.createElement('div');
    pelicula.classList.add('pelicula');
    contenedor.appendChild(pelicula);

    var titulo = document.createElement('div');
    titulo.innerHTML = objeto.title;
    titulo.classList.add('pelicula__titulo');
    pelicula.appendChild(titulo);

    var descripcion = document.createElement('div');
    descripcion.innerHTML = objeto.description;
    descripcion.classList.add('pelicula__descripcion');
    pelicula.appendChild(descripcion);

    var imagendiv = document.createElement('div');
    pelicula.appendChild(imagendiv);
    var imagen = document.createElement('img');
    imagen.src = 'little.png';
    imagendiv.appendChild(imagen);

    app.appendChild(contenedor);
  });
}
request.send()
